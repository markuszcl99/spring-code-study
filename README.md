# 前言
> 一个普通的Java程序员，对Spring有比较高的兴趣，于是通过搜集资料、查看视频的方式，将Spring源码分析记录成笔记的方式，
> 现在开源出来，分享给大家。目前规划是以笔记的形式分享给大家，待自己掌握的差不多了，再将这些知识点以视频的方式给大家见面！
> 总结不易，给个star呗，嘻嘻！现在还是个小菜鸟，如果有错误的地方，不吝赐教！抱拳了哈哈！
# 阅读方式
> 此开源项目有代码+笔记，代码都是自己执行过的，保证好使，建议大家在此页面，也就是跟着README.md文章按顺序阅读，下面的目录都是超链接，
> 如果点击标题不跳转说明还没有更新，菜鸟会抓紧更新哈。笔记中涵盖代码，如果自己想运行一遍，建议自己手打，如果图省事，可以按照我每篇笔记里记录的代码地址，进行复制粘贴到自己的项目中
# Spring源码学习（附目录）
Spring IoC系列

第一章：Spring Framework总览

01 | Spring 版本特性：Spring各个版本引入了哪些新特征？

02 | Spring 模块化设计：Spring功能特性如何在不同模块中组织？

03 | Java 语言特性运用：各种Java语法特性是怎样被Spring各种版本巧妙运用的？

04 | JDK API 实践：Spring怎样取舍Java、I/O、集合、反射、动态代理等API的使用？

05 | Java EE API 整合：为什么Spring要与“笨重”的Java EE共舞？

06 | Spring 编程模型：Spring实现了哪些编程模型？

07 | Spring 核心价值：我们能从Spring Framework中学到哪些经验和教训呢？

第二章：重新认识IoC

08 | IoC发展简介：你可能对IoC有些误会？

09 | IoC主要实现策略：面试官总问IoC和DI的区别，他真的理解吗？

10 | IoC容器的职责：IoC除了依赖注入，还涵盖哪些职责呢？

11 | 除了Spring，还有其他的IoC容器实现吗？

12 | 传统的IoC容器实现：JavaBeans也是IoC容器吗？

13 | 轻量级IoC容器：如何界定IoC容器的“轻重”？

14 | 依赖查找 VS 依赖注入：为什么Spring总会强调后者，而选择性忽略前者？

15 | 构造器注入 VS Setter注入：为什么Spring官方文档的解读与作者的初心出现偏差？

第三章：Spring IoC容器概述

[16 | Spring IoC依赖查找：依赖注入还不够吗？依赖查找存在的价值几何？](https://gitee.com/markuszcl99/spring-code-study/blob/master/%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9ASpring%20IoC%E5%AE%B9%E5%99%A8%E6%A6%82%E8%BF%B0/Spring%20IoC%E4%BE%9D%E8%B5%96%E6%9F%A5%E6%89%BE.md)

[17 | Spring IoC依赖注入：Spring提供了哪些依赖注入模式和类型呢？](https://gitee.com/markuszcl99/spring-code-study/blob/master/%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9ASpring%20IoC%E5%AE%B9%E5%99%A8%E6%A6%82%E8%BF%B0/Spring%20IoC%E4%BE%9D%E8%B5%96%E6%B3%A8%E5%85%A5.md)

[18 | Spring IoC依赖来源：依赖注入和查找的对象来自于哪里？](https://gitee.com/markuszcl99/spring-code-study/blob/feature-chapter-of-third-20210311/%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9ASpring%20IoC%E5%AE%B9%E5%99%A8%E6%A6%82%E8%BF%B0/Spring%20IoC%E4%BE%9D%E8%B5%96%E6%9D%A5%E6%BA%90.md)

[19 | Spring IoC配置元信息：Spring IoC有哪些配置元信息？它们的进化过程是怎样的？](https://gitee.com/markuszcl99/spring-code-study/blob/feature-chapter-of-third-20210311/%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9ASpring%20IoC%E5%AE%B9%E5%99%A8%E6%A6%82%E8%BF%B0/Spring%20IoC%20%E9%85%8D%E7%BD%AE%E5%85%83%E4%BF%A1%E6%81%AF.md)

[20 | Spring IoC容器：BeanFactory和ApplicationContext谁才是Spring IoC容器？](https://gitee.com/markuszcl99/spring-code-study/blob/feature-chapter-of-third-20210311/%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9ASpring%20IoC%E5%AE%B9%E5%99%A8%E6%A6%82%E8%BF%B0/Spring%20IoC%20%E8%B0%81%E6%89%8D%E6%98%AF%E5%AE%B9%E5%99%A8.md)

[21 | Spring IoC应用上下文：ApplicationContext除了IoC容器角色，还提供哪些特性？](https://gitee.com/markuszcl99/spring-code-study/blob/feature-chapter-of-third-20210311/%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9ASpring%20IoC%E5%AE%B9%E5%99%A8%E6%A6%82%E8%BF%B0/Spring%20IoC%20%E5%BA%94%E7%94%A8%E4%B8%8A%E4%B8%8B%E6%96%87.md)

[22 | 使用Spring IoC容器：选BeanFactory还是ApplicationContext？](https://gitee.com/markuszcl99/spring-code-study/blob/feature-chapter-of-third-20210311/%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9ASpring%20IoC%E5%AE%B9%E5%99%A8%E6%A6%82%E8%BF%B0/Spring%20IoC%20%E5%A6%82%E4%BD%95%E9%80%89%E6%8B%A9.md)

[23 | Spring IoC容器生命周期：IoC容器启停过程中发生了什么？](https://gitee.com/markuszcl99/spring-code-study/blob/master/%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9ASpring%20IoC%E5%AE%B9%E5%99%A8%E6%A6%82%E8%BF%B0/Spring%20IoC%E7%94%9F%E5%91%BD%E5%91%A8%E6%9C%9F.md)

[第三章总结](https://gitee.com/markuszcl99/spring-code-study/blob/master/%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9ASpring%20IoC%E5%AE%B9%E5%99%A8%E6%A6%82%E8%BF%B0/%E7%AC%AC%E4%B8%89%E7%AB%A0%E6%80%BB%E7%BB%93.md)

第四章：Spring Bean基础

24 | 定义Bean：什么是BeanDefinition？

25 | BeanDefinition元信息：除了Bean名称和类名，还有哪些Bean元信息值得关注？

26 | 命名Spring Bean：id和name属性命名Bean，哪个更好？

27 | Spring Bean的别名：为什么命名Bean还需要别名？

28 | 注册Spring Bean：如何将BeanDefinition注册到IoC容器？

29 | 实例化Spring Bean：Bean实例化的姿势有多少种？

30 | 初始化Spring Bean：Bean初始化有哪些方式？

31 | 延迟初始化Spring Bean：延迟初始化的Bean会影响依赖注入吗？

32 | 销毁Spring Bean：销毁Bean的基本操作有哪些？

33 | 回收Spring Bean：Spring IoC容器管理的Bean能够被垃圾回收吗？

Spring AOP系列


