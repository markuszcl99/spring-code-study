package com.markus.spring.ioc.dependency.domain;

import com.markus.spring.ioc.dependency.annotation.Super;

/**
 * @author 14222
 * @date 2021-02-02 21:49
 */
@Super
public class SuperUser extends User {
    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "SuperUser{" +
                "address='" + address + '\'' +
                "} "+super.toString();
    }
}
