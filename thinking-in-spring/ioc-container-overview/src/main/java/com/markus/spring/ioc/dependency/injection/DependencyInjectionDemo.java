package com.markus.spring.ioc.dependency.injection;

import com.markus.spring.ioc.dependency.repository.UserRepository;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.env.Environment;

/**
 * @author 14222
 * @date 2021-02-02 22:07
 */
public class DependencyInjectionDemo {
    public static void main(String[] args) {
        //1.配置 xml 文件
        //2.启动 Spring 应用上下文
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:/META-INF/context-dependency-injection.xml");

        UserRepository repository = applicationContext.getBean(UserRepository.class);
//        System.out.println(repository.getUsers());
//        System.out.println(repository.getBeanFactory() == beanFactory);
//        System.out.println(repository.getBeanFactory());
//        System.out.println("repository.getObjectFactory().getObject() = beanFactory ? " + (repository.getObjectFactory().getObject() == beanFactory));
        // 错误依赖查找
//        System.out.println(beanFactory.getBean(BeanFactory.class));

//        Environment environment = applicationContext.getBean(Environment.class);
//        System.out.println(environment);
        whoIsIoCContainer(repository,applicationContext);
    }

    private static void whoIsIoCContainer(UserRepository userRepository,ApplicationContext applicationContext){

        // ConfigurableApplicationContext <- ApplicationContext <- BeanFactory
        // ConfigurableApplicationContext#getBeanFactory()

        System.out.println(userRepository.getBeanFactory() == applicationContext);
    }
}
