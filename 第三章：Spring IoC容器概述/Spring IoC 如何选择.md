### 22 | 使用Spring IoC容器：选BeanFactory还是ApplicationContext？

BeanFactory作为Spring 底层IoC容器，ApplicationContext作为IoC容器具备更多特性，那么Spring在选择IoC容器的时候，哪些场景会选择BeanFactory，哪些场景会选择ApplicationContext呢？我们这一节通过两种场景来讨论如何在BeanFactory和ApplicationContext做选择！

- 我们知道，真正的BeanFactory实例对象其实是 DefaultListableBeanFactory，所以我们写一个demo，手动创建一个DefaultListableBeanFactory，然后根据这个新的BeanFactory创建一个新的XMLBeanDefinitionReader（xml bean 定义读取器），我们来看下代码

  - ```java
    import org.springframework.beans.factory.support.DefaultListableBeanFactory;
    import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
    /**
     * @link{ BeanFactory } xml 定义 Bean
     * @author 14222
     * @date 2021-03-13 13:46
     */
    public class BeanFactoryAsIoCContainerDemo {
        public static void main(String[] args) {
            // 创建 BeanFactory 容器
            DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
            XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(beanFactory);
            // Xml 配置文件路径
            String location = "classpath:/META-INF/context-dependency-lookup.xml";
            // 读取我们指定的 xml 配置文件
            int beanDefinitionsCount = reader.loadBeanDefinitions(location);
            // 我们指定的 xml 配置文件里有三个 Bean 定义，分别是 User、SuperUser、ObjectFactory
            System.out.println(beanDefinitionsCount);
        }
    }
    ```

  - 控制台打印的数字应为 3，实际上也是的。

  - <img src="https://gitee.com/markuszcl99/images/raw/master/20210313140030.png" style="zoom:150%;" />

  - 继续更新Demo，将 xml 中定义的User实例的Bean在控制台中打印出来

  - ```java
    public class BeanFactoryAsIoCContainerDemo {
        public static void main(String[] args) {
            DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
            XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(beanFactory);
            String location = "classpath:/META-INF/context-dependency-lookup.xml";
            int beanDefinitionsCount = reader.loadBeanDefinitions(location);
            System.out.println("Bean 定义的数量"+beanDefinitionsCount);
            printCollectionBeanByType(beanFactory);
        }
    
        private static void printCollectionBeanByType(DefaultListableBeanFactory beanFactory) {
            if (beanFactory instanceof ListableBeanFactory){
                ListableBeanFactory listableBeanFactory = beanFactory;
                Map<String, User> beansOfType = listableBeanFactory.getBeansOfType(User.class);
                System.out.println(beansOfType);
            }
        }
    }
    ```

  - ![](https://gitee.com/markuszcl99/images/raw/master/20210313140707.png)

- 接下来换另一种场景，当我们使用Java注解定义Bean时，Spring底层是怎么实现的！

  - 这里又会出现一个新的对象：AnnotationConfigApplicationContext

  - ```java
    import com.markus.spring.ioc.dependency.domain.User;
    import org.springframework.beans.factory.BeanFactory;
    import org.springframework.beans.factory.ListableBeanFactory;
    import org.springframework.beans.factory.support.DefaultListableBeanFactory;
    import org.springframework.context.annotation.AnnotationConfigApplicationContext;
    import org.springframework.context.annotation.Bean;
    import java.util.Map;
    /**
     * 支持注解功能 @link{ApplicationContext} 注解定义Bean
     * @author 14222
     * @date 2021-03-13 14:09
     */
    public class AnnotationApplicationContextAsIoCContainerDemo {
        public static void main(String[] args) {
            AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
            // 注册扫描类
            applicationContext.register(AnnotationApplicationContextAsIoCContainerDemo.class);
            // 启动上下文容器
            applicationContext.refresh();
            printCollectionBeanByType(applicationContext);
        }	
        @Bean
        public User user(){
            User user = new User();
            user.setId(2L);
            user.setName("markus");
            return user;
        }
    
        private static void printCollectionBeanByType(BeanFactory beanFactory) {
            if (beanFactory instanceof ListableBeanFactory){
                ListableBeanFactory listableBeanFactory = (ListableBeanFactory) beanFactory;
                Map<String, User> beansOfType = listableBeanFactory.getBeansOfType(User.class);
                System.out.println(beansOfType);
            }
        }
    }
    
    ```

  - <img src="https://gitee.com/markuszcl99/images/raw/master/20210313141640.png" style="zoom:150%;" />

  - 这个就是将我们使用@Bean定义的Bean打印出来了，我们的Demo是新建了一个AnnotationConfigApplicationContext，然后将我们的Demo注册到该容器当中，然后启动上下文容器，生成Bean对象，然后控制台打印出来

- 其实第一种场景就能实现Bean的注入和查找，如果注入和查找Bean足以满足你的业务，那么BeanFactory就可以胜任，但是就没有了前面所说的事件或者资源管理异或是国际化等等的功能了，而第二种场景便是支持多种特性的ApplicationContext的一个子集，它支持注解，还有支持其他功能的ApplicationContext就不在这罗列了。

看到第二个Demo的时候，必须要写容器启动的代码，程序才能正常运行，那么IoC容器在启停过程中发生了什么？大家是不是很好奇，那么下一节就会大概讲一下IoC容器的生命周期！那么，还等什么？赶紧下一节，冲！