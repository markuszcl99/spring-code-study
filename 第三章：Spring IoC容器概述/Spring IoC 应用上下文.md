### 21 | Spring IoC应用上下文：ApplicationContext除了IoC容器角色，还提供哪些特性？

前面讲到BeanFactory和ApplicationContext的区别，他们都是IoC容器，但ApplicationContext除了IoC容器角色之外，还有哪些特性呢？查阅资料，大致可以罗列这些特性

- 面向切面（AOP）
- 配置元信息（Configuration Metadata）
- 资源管理（Resources）
- 事件（Event）
- 国际化（i18n）
- 注解（Annotation）
- Environment抽象（Environment Abstraction）