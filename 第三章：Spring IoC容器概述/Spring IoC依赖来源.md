### 18 | Spring IoC依赖来源：依赖注入和查找的对象来自于哪里？

#### 00、代码实现参考包的路径

> 同17节

#### 01、依赖来源总结

查阅资料发现，其实Spring IoC依赖的来源大致分为两个方向：一种是Bean对象，另一种就是非Bean对象。具体可分为以下几种：

- 自定义Bean

  - 其实就是我们业务需要的Bean，需要我们手动去配置，17节已经给大家讲述了。

- Spring 内建Bean

  - 内建Bean的话，对大家可能是一个新词，我用一个案例来给大家解释：

  - 借着17节的Demo继续给大家更新

  - ```java
    public class DependencyInjectionDemo {
        public static void main(String[] args) {
            //1.配置 xml 文件
            //2.启动 Spring 应用上下文
            BeanFactory beanFactory = new ClassPathXmlApplicationContext("classpath:/META-INF/context-dependency-injection.xml");
    
    //        UserRepository repository = beanFactory.getBean(UserRepository.class);
    //        System.out.println(repository.getUsers());
    //        System.out.println(repository.getBeanFactory() == beanFactory);
    //        System.out.println(repository.getBeanFactory());
    //        System.out.println("repository.getObjectFactory().getObject() = beanFactory ? " + (repository.getObjectFactory().getObject() == beanFactory));
            // 错误依赖查找
    //        System.out.println(beanFactory.getBean(BeanFactory.class));
    
            Environment environment = beanFactory.getBean(Environment.class);
            System.out.println(environment);
        }
    }
    ```

  - ![](https://gitee.com/markuszcl99/images/raw/master/20210312194722.png)

  - Environment是我们新接触的一个Bean，可能看着眼生，但实际我们经常做一些事情离不开他，也就是 外部化配置 和 profile来结合使用时就会涉及到这个Environment。它是Spring 内建Bean

- Spring 内建依赖

  - 这个就回到17节，我给大家说的BeanFactory注入到IoC容器中真实的对象：DefaultListableBeanFactory，它其实就是Spring内建的依赖，而非Bean对象。
#### 小提示：
为方便大家观看，看完本节可以直接返回上一节或者跳到下一节 [上一节](https://gitee.com/markuszcl99/spring-code-study/blob/feature-chapter-of-third-20210311/%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9ASpring%20IoC%E5%AE%B9%E5%99%A8%E6%A6%82%E8%BF%B0/Spring%20IoC%E4%BE%9D%E8%B5%96%E6%B3%A8%E5%85%A5.md)   [下一节]()；