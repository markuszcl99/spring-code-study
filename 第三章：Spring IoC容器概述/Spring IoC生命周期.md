### 23 | Spring IoC容器生命周期：IoC容器启停过程中发生了什么？

在本节开始之前，先给大家说明一下，该节并不会详细的去讲 IoC 容器的生命周期细节，而仅仅是停留在表面的工作，告诉大家IoC大致做了哪些工作，抠细节的工作是非常复杂的，等到我们把这些基础学会了，再去深挖细节！

正文开始之前，先给大家说下本节我的分析方式：

- 首先，我会贴出代码，让大家总览一下整体的流程
- 接着，再试图进入到某个操作中，大概说一下里面的内容
- 最后，总结整个流程

好了，正文开始，大家还记得我们上节说到的 AnnotationConfigApplicationContext 吗？我们在代码中如何使用的呢？我把代码截图过来：

![](https://gitee.com/markuszcl99/images/raw/master/20210313192006.png)

图中用红框框出的那段代码，是我们本篇的入口，它的功能就是启动上下文，我们点进去看看，它都做了些什么！

- ```java
  public void refresh() throws BeansException, IllegalStateException {
      synchronized (this.startupShutdownMonitor) {
          // Prepare this context for refreshing.
          // 在启动时，做一下准备工作；
          // 这个方法里面做了一些小事情，比如：
          // 1、记录容器启动的开始时间
          // 2、调整该容器的flag，也就是状态，从 关闭状态(close) 调整为 活跃状态(close);
          // 3、初始化environment外部化配置内容，处理一些占位符，比如使用@Value修饰的属性
          // 4、验证所有标记为required的属性都是可解析的，具体方法实现参考ConfigurablePropertyResolver#setRequiredProperties这个接口
          prepareRefresh();
  
          // Tell the subclass to refresh the internal bean factory.
          // 初看这块的英文注释的时候是不是很懵逼？我看了好长一会才反应过来，意思是说：告诉所有子类（也就是所有继承AbstractApplicationContext的类），他们都是需要做这件事情，而且是必须的！也就是获取Spring内建BeanFactory：ConfigurableListableBeanFactory，是不是很熟悉，它就是我们前几节提到的Spring内建依赖DefaultListableBeanFactory的父接口
          ConfigurableListableBeanFactory beanFactory = obtainFreshBeanFactory();
  
          // Prepare the bean factory for use in this context.
          // 配置该BeanFactory的标准上下文的特性，例如该上下文的类加载器和后置处理器，这个方法的流程难以被理解，我认为还是比较复杂的，我们后期再做解析
          prepareBeanFactory(beanFactory);
  
          try {
              // Allows post-processing of the bean factory in context subclasses.
              // 这个可以做进一步的初始化，也就是说，如果我们想要在初始化BeanFactory的时候做些事情，BeanFactoryPostProcessor，然后实现此类里的postProcessBeanFactory方法，然后上下文在执行到此处的时候会回调你重写的方法内容！
              postProcessBeanFactory(beanFactory);
  
              // Invoke factory processors registered as beans in the context.
              // 对所有定义的后置处理器进行调用，也就是执行我们自定义的postProcessor的方法
              invokeBeanFactoryPostProcessors(beanFactory);
  
              // Register bean processors that intercept bean creation.
              // 这个就是对Bean进行调整，这步以上的步骤可以理解为对容器的调整！
              registerBeanPostProcessors(beanFactory);
  
              // Initialize message source for this context.
              // 这个就是初始化 国际化 方面的特性，在这块也能看出，我们上节提到的BeanFactory和ApplicationContext的不同，ApplicationContext能够提供应用级别的国际化特性
              initMessageSource();
  
              // Initialize event multicaster for this context.
              // 执行完这行代码，就可以说 上下文具备了 事件传播 的一些特性！
              initApplicationEventMulticaster();
  
              // Initialize other special beans in specific context subclasses.
              onRefresh();
  
              // Check for listener beans and register them.
              registerListeners();
  
              // Instantiate all remaining (non-lazy-init) singletons.
              finishBeanFactoryInitialization(beanFactory);
  
              // Last step: publish corresponding event.
              finishRefresh();
          }
  
          catch (BeansException ex) {
              if (logger.isWarnEnabled()) {
                  logger.warn("Exception encountered during context initialization - " +
                              "cancelling refresh attempt: " + ex);
              }
  
              // Destroy already created singletons to avoid dangling resources.
              destroyBeans();
  
              // Reset 'active' flag.
              cancelRefresh(ex);
  
              // Propagate exception to caller.
              throw ex;
          }
  
          finally {
              // Reset common introspection caches in Spring's core, since we
              // might not ever need metadata for singleton beans anymore...
              resetCommonCaches();
          }
      }
  }
  ```

- 看完代码之后，我们可以总结一下，Spring IoC容器在启动的时候做了些什么！

  - 首先，它先创建了BeanFactory，并且对它进行初步的初始化，包括对它加入一些内建的Bean对象或者内建的依赖
  - 接着，就是BeanFactory的拓展点，通过BeanFactoryPostProcessor来进行执行
  - 然后，对Bean的一些修改或者扩展，通过registerBeanPostProcessors来进行执行，这里只是注册，具体的调用是在BeanFactory中进行的，有兴趣的同学可以去看看
  - 在接下来就是初始化 ApplicationContext的高级特性，例如：国际化、事件、资源等

Spring IoC容器在启动的时候，大致走了以上那么些流程，IoC容器在运行时，就正常执行我们的业务代码，没什么说的，接下来我们就来看看Spring IoC容器在停止的时候做了些什么吧！继续补充完善我们的Demo！

![](https://gitee.com/markuszcl99/images/raw/master/20210313202120.png)

与之前不同的是，我们在程序的最后加入了关闭的行为，这也我们将要分析的入口！

- 它的内容还是比较简单的，一句话概括就是：关闭上下文，销毁在这个BeanFactory里的所有的Bean对象

- ```java
  public void close() {
      synchronized (this.startupShutdownMonitor) {
          // 重点在这里
          doClose();
          // If we registered a JVM shutdown hook, we don't need it anymore now:
          // We've already explicitly closed the context.
          if (this.shutdownHook != null) {
              try {
                  Runtime.getRuntime().removeShutdownHook(this.shutdownHook);
              }
              catch (IllegalStateException ex) {
                  // ignore - VM is already shutting down
              }
          }
      }
  }
  ```

- close()的重点在于doClose()方法，进入到里面，我们看看他做了些什么！

- ```java
  protected void doClose() {
      // Check whether an actual close attempt is necessary...
      if (this.active.get() && this.closed.compareAndSet(false, true)) {
          if (logger.isDebugEnabled()) {
              logger.debug("Closing " + this);
          }
  
          LiveBeansView.unregisterApplicationContext(this);
  
          try {
              // Publish shutdown event.
              publishEvent(new ContextClosedEvent(this));
          }
          catch (Throwable ex) {
              logger.warn("Exception thrown from ApplicationListener handling ContextClosedEvent", ex);
          }
  
          // Stop all Lifecycle beans, to avoid delays during individual destruction.
          if (this.lifecycleProcessor != null) {
              try {
                  this.lifecycleProcessor.onClose();
              }
              catch (Throwable ex) {
                  logger.warn("Exception thrown from LifecycleProcessor on context close", ex);
              }
          }
  
          // Destroy all cached singletons in the context's BeanFactory.
          // 销毁所有的Bean
          destroyBeans();
  
          // Close the state of this context itself.
          // 关闭Bean工厂
          closeBeanFactory();
  
          // Let subclasses do some final clean-up if they wish...
          // 还有进一步的close，这个就是留给我们子类去执行的，目前Spring内建还在此没有做任何事情，如果开发者在容器关闭的时候有业务需求的话，可以重写此方法，在执行的时候进行回调！
          onClose();
  
          // Reset local application listeners to pre-refresh state.
          if (this.earlyApplicationListeners != null) {
              this.applicationListeners.clear();
              this.applicationListeners.addAll(this.earlyApplicationListeners);
          }
  
          // Switch to inactive.
          this.active.set(false);
      }
  }
  ```

好了，Spring IoC容器的生命周期就简单的给大家介绍了一下，由于它的整体流程是非常复杂的，在这里就不给大家一一介绍了，我们继续往下看，等到我们把整个Spring IoC的基础一一攻破之后再回头看整个生命周期，那我们就能记住其中的门道，而不至于很快忘记。我们的目的不仅仅是像背八股文那样记住这些流程，而是需要真真正正的去理解它！